import React, {Component} from 'react';
import {View, Text} from 'react-native';

class ExampleScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View>
        <Text> ExampleScreen </Text>
      </View>
    );
  }
}

export default ExampleScreen;
